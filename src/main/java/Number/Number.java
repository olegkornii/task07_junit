package Number;

public class Number {
    int value;

    public static int square(int value) {
        value = value * value;
        return value;
    }

    public static int squareRoot(int value) {
        return (int) Math.sqrt(value);
    }

    public static boolean isNatural(int value) {
        return value >= 0;
    }

    public static boolean isSimple(int value) {
        for (int i = 1; i < value; i++) {
            if (i == 1) continue;
            if (value % i == 0) return false;
        }
        return true;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
