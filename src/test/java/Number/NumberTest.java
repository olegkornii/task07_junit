package Number;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberTest {
    @BeforeAll
    static void before() {
        System.out.println("Before testing");
    }

    @Test
    void square() {
        assertEquals(16, Number.square(4));
        System.out.println("Square works successful");

    }

    @Test
    void squareRoot() {
        assertEquals(5, Number.squareRoot(25));
        System.out.println("Square root works successful");
    }

    @Test
    void isSimple() {
        assertTrue(Number.isSimple(11));
        System.out.println("IsSimple works successful");
    }

    @Test
    void isNatural() {
        assertTrue(Number.isNatural(2));
        System.out.println("isNatural works successful");
    }

    @AfterAll
    static void after() {
        System.out.println("After testing");
    }
}